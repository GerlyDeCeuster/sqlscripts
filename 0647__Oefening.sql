USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleasesLoop` (in extraReleases int)
BEGIN
	declare counter int default 0;
    declare succes bool;
    callloop: LOOP
    call MockAlbumReleaseWithSucces (succes);
    if succes = 1 then 
		set counter = counter+1;
	end if;
	if counter = extraReleases then
		leave callloop;
	end if;
	end loop;
END$$
DELIMITER ;