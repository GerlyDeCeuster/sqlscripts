use modernways;
create table Uitleningen 
(BeginDatum DATE not null,
EindDatum DATE,
Boeken_Id INT not null,
Leden_Id INT not null,
constraint fk_Uitleningen_Boeken foreign key (Boeken_Id) references Boeken(Id),
constraint fk_Uitleningen_Leden foreign key (Leden_Id) references Leden (Id));