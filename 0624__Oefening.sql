-- toont combinaties van liedjes en bands
-- doet dit enkel voor liedjestitels die beginnen met 'A'
-- gaat van kort naar lang
SELECT Titel, Naam, Lengte FROM Liedjes
inner join Bands
on Liedjes.Bands_Id = Bands.Id
where Titel like 'A%'
order by Lengte;
-- execution time= "0:00:1.90019920"
-- execution time 2="0:00:0.12734650"
create index TitelIdx
on Liedjes (Titel);