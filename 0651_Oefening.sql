use aptunes;

delimiter $$
create procedure GetAlbumDuration (in album int, out totalDuration smallint unsigned) 
begin 
	declare songDuration tinyint unsigned default 0;
    declare ok bool default false;
	declare songDurationCursor cursor for select Lengte from Liedjes where Albums_Id = album;
    declare continue handler for not found set ok= true;
    set totalDuration = 0;
    open songDurationCursor;
    fetchloop: loop
		fetch songDurationCursor into songDuration;
        if ok= true then 
			leave fetchloop; 
        end if;
        set totalDuration= totalDuration + songDuration;
    end loop;
    close songDurationCursor;
end$$
delimiter ;