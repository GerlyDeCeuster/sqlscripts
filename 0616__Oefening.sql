use modernways;

CREATE VIEW GemiddeldeRatings
AS
SELECT Boeken_Id, avg(Rating) as Rating
FROM reviews
group by Boeken_Id;
