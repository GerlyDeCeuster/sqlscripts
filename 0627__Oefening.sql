use ModernWays;

select Voornaam
from studenten
where length(Voornaam) <= (select avg(length(Voornaam)) from studenten);