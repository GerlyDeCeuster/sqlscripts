select count(*)
from muzikanten;

select count(distinct left(concat(Voornaam,' ',Familienaam),20))
from muzikanten;

create index VoornaamFamilienaamIdx
on muzikanten(Voornaam, Familienaam);