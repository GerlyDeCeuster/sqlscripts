use aptunes;
DROP procedure IF EXISTS `GetLiedjes`;

DELIMITER $$
USE aptunes$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetLiedjes`(IN nameTitel VARCHAR(50))
BEGIN
    SELECT Titel
    FROM liedjes
    WHERE Titel LIKE CONCAT('%',nameTitel,'%');
END$$

DELIMITER ;

Call GetLiedjes('web')