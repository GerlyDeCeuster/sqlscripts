use aptunes;
DROP procedure IF EXISTS `NumberOfGenres`;

DELIMITER $$
USE aptunes$$
CREATE PROCEDURE `NumberOfGenres`(OUT Aantal tinyint )
BEGIN
    SELECT count(*)
    into Aantal
    FROM genres;
END$$

CALL NumberOfGenres(@Aantal)