use modernways;

CREATE VIEW AuteursBoeken
AS
SELECT CONCAT(Voornaam," ",Familienaam) as Auteur, Titel
FROM personen
inner JOIN publicaties ON personen.Id = Personen_Id
inner join boeken on boeken.Id=Boeken_Id;