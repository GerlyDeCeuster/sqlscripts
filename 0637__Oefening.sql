use ModernWays;

-- creëren van een tabel personen met Voornaam en familienaam als kolommen
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- De kolommen worden ingevuld met de data uit tabel Boeken. Met distinct worden de dubbele weggelaten.
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;

-- Er worden meerdere kolommen toegevoegd evenals een primary key   
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);

-- Er wordt een kolom met toegevoegd aan tabel Boeken die later zal dienen als foreign key.
alter table Boeken add Personen_Id int null;

-- De id van Personen wordt gekopiëerd in de tabel Boeken waarbij voornaam en familienaam gelijk zijn in beide tabellen. 
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;

-- We veranderen kolom Personen_Id zodat deze niet 0 mag zijn.
alter table Boeken change Personen_Id Personen_Id int not null;

-- Overbodige kolommen worden verwijderd uit tabel Boeken
alter table Boeken drop column Voornaam,
    drop column Familienaam;

-- We maken van kolom Peronen_Id een echte foreign key
 alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);   

