USE `aptunes`;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CreateAndReleaseAlbum` (IN titel varchar(100), IN bands_Id INT)
BEGIN
start transaction;
    insert into Albums(Titel)
    values(titel);
    insert into Albumreleases(Bands_Id,Albums_Id)
    values (bands_Id, last_insert_id());
    commit;
END$$
DELIMITER ;