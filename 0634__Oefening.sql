use ModernWays;

select min(GemiddeldeCijfer) as Gemiddelde
from
(select avg(cijfer) as GemiddeldeCijfer
 from studenten inner join evaluaties on Id=Studenten_Id
 group by Id) as GemiddeldeCijfers;