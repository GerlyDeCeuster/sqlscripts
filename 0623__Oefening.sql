-- toont alle geldige combinaties van liedjestitels en genres
select Titel, Naam
from Liedjesgenres inner join Liedjes
on Liedjesgenres.Liedjes_Id = Liedjes.Id
inner join Genres
on Liedjesgenres.Genres_Id = Genres.Id;

-- executiontime:'0:00:4.55261750'
-- executiontime 2:'0:00:5.61542420'

create index NaamIdx
on Genres(Naam);

create index TitelIdx
on Liedjes(Titel);