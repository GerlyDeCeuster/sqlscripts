use modernways;

SELECT Leeftijdscategorie, AVG(Loon) AS 'Gemiddeld loon' FROM 
(select floor(leeftijd/10)*10 as 'Leeftijdscategorie', Id
from personeelsleden) as Leeftijdscategorie
inner join personeelsleden on personeelsleden.Id= leeftijdscategorie.Id
group by Leeftijdscategorie
order by Leeftijdscategorie;
 
