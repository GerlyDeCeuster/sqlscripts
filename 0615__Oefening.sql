use modernways;

ALTER VIEW auteursboeken
AS
SELECT CONCAT(Voornaam," ",Familienaam) as Auteur, Titel, boeken.Id as Boeken_Id
FROM personen
inner JOIN publicaties ON personen.Id = Personen_Id
inner join boeken on boeken.Id=Boeken_Id;