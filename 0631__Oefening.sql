use ModernWays;

select distinct Voornaam
from studenten
where Voornaam in
		(select Voornaam from personeelsleden
         where Voornaam in 
			(select Voornaam from directieleden));