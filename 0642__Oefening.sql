USE `aptunes`;
DROP procedure IF EXISTS `CleanupOldMemberships`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CleanupOldMemberships` (IN enddate date, out num INT)
BEGIN
start transaction;
	select count(*) into num
    from Lidmaatschappen
    where Lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < enddate;
    set sql_safe_updates=0;
    delete from Lidmaatschappen
    where Lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < enddate;
    set sql_safe_updates=1;
commit;
END$$
DELIMITER ;