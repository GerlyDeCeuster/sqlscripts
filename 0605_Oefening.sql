USE ModernWays;

INSERT INTO Uitleningen (BeginDatum, EindDatum, Leden_Id, Boeken_Id) 
VALUES
-- Begin- en einddatum, Max = Id 3, Norwegian Wood = Id 1
('2019-02-01', '2019-02-15', 3, 1),
-- de rest...
('2019-02-16', '2019-03-02', 2, 1),
('2019-02-16', '2019-03-02', 2, 5),
('2019-05-01', NULL, 1, 5);