USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DemonstrateHandlerOrder`()
BEGIN
	declare randomValue tinyint default 0;
    declare continue handler for sqlstate '45002'
		begin
			select 'State 45002 opgevangen. Geen probleem.';
		end;
	declare continue handler for sqlexception
		begin 
			select 'Een algemene fout opgevangen.';
		end;
	set randomValue= FLOOR(RAND() * 3) + 1;
    if randomValue = 1 then
		SIGNAL SQLSTATE '45001';
	elseif randomValue=2 then
		SIGNAL SQLSTATE '45002';
	else
		SIGNAL SQLSTATE '45003';
	end if;	
END$$

DELIMITER ;